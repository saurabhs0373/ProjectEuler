#include <iostream>
#include <conio.h>

int SumOfMultiples(int limit)
{
	int sum = 0;
	
	for(int i = 0; i < limit; i ++)
		if(i % 3 == 0 || i % 5 == 0)
			sum += i;

	return sum;
}

int main()
{
	printf_s("sum %d ", SumOfMultiples(1000));

	_getch();

	return 0;
}