#include <iostream>
#include <conio.h>

bool IsLeapYear(int year)
{
	return (year % 4 == 0 ? (year % 100 == 0 ? (year % 400 == 0 ? true : false) : true) : false);
}

int NoOfSundays(int startDay, int startYear, int endYear)
{
	int noOfSundays = 0;
	int months[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	int month = 0;
	int year = startYear;
	int day = startDay;

	while(year <= endYear)
	{
		if(day == 0)
			noOfSundays ++;

		day += months[month] % 7;
		day %= 7;
		
		month ++;

		if(month > 11)
		{
			month = 0;
			year ++;	
			months[1] = IsLeapYear(year) ? 29 : 28;
		}
	}

	return noOfSundays;
}

int main()
{
	std::cout << "NoOfSundays " << NoOfSundays(2, 1901, 2000);

	_getch();
	return 0;
}