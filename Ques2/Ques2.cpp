#include <iostream>
#include <conio.h>

void GetFibonacci(int& var1, int& var2)
{
	int temp = var1 + var2;
	var1 = var2;
	var2 = temp;
}

int SumOfEvenFibonacci(int limit)
{
	int sum = 0;
	int var1 = 0;
	int var2 = 1;

	while(var2 < limit)
	{
		GetFibonacci(var1, var2);

		if(var1 % 2 == 0)
			sum += var1;
	}

	return sum;
}

int main()
{
	printf_s("\n\nSum %d", SumOfEvenFibonacci(4000000));

	_getch();
	return 0;
}