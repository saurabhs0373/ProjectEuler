#include <iostream>
#include <conio.h>

bool IsPrime(long long  var)
{
	if(var == 2)
		return true;

	for(long long i = 2; i <= var / 2; i ++)
	{
		if(var % i == 0)
			return false;
	}

	return true;
}

long long LargestPrimeFactor(long long var)
{
	long long maxPrime = 0;
	long long temp = var;

	for(long long i = var / 2; i >= 2; i--)
	{
		if(var % i == 0)
		{
			if(IsPrime(i))
			{
				return i;
			}

			temp /= i;
		}
	}

	return var;
}

int main()
{
	std::cout << "\n\nLargest Prime " << LargestPrimeFactor(600851475143);

	_getch();
	return 0;
}