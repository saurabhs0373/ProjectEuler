#include <iostream>
#include <conio.h>

int AdditionOfPowers(int power)
{
	int num = 2;
	int sumAll = 0;

	while(num < pow(10, power + 1))
	{
		int tempNum = num;
		int sum = 0;
		while(tempNum > 0)
		{
			int lastDigit = tempNum % 10;
			sum += pow(lastDigit, power);
			tempNum /= 10;
		}

		if(sum == num)
			sumAll += sum;

		num++;
	}

	return sumAll;
}

int main()
{
	std::cout << "Addition of Powers " << AdditionOfPowers(5);

	_getch();
	return 0;
}