#include <iostream>
#include <conio.h>

bool IsPallindrome(int var)
{
	int temp = var;
	int reverse = 0;

	while(temp > 0)
	{
		reverse *= 10;
		reverse += temp % 10;
		temp /= 10;
	}

	return reverse == var;
}

int LargestPallindrome(int noOfDigit)
{
	int largest = 0;

	int max = pow(10, noOfDigit);
	int min = pow(10, noOfDigit - 1);

	for(int i = max; i >= min; i --)
	{
		for(int j = max; j >= min; j --)
		{
			int product = i * j;
			if(IsPallindrome(product))
			{
				if(product > largest)
				{
					largest = product;
				}
			}
		}
	}

	return largest;
}

int main()
{
	std::cout << "Largest Pallindrome " << LargestPallindrome(3);

	_getch();
	return 0;
}