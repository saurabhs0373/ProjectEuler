#include <iostream>
#include <conio.h>

bool IsPrime(int var)
{
	if(var == 1)
		return false;
	if(var == 2)
		return true;

	for(int i = 2; i <= var / 2; i ++)
	{
		if(var % i == 0)
			return false;
	}

	return true;
}

int NthPrimeNumber(int nth)
{
	int index = 0;
	int num = 0;

	do
	{
		num ++;

		if(IsPrime(num))
		{
			index ++;
		}
		
	}while(index < nth);

	return num;
}

int main()
{
	std::cout << "Nth Prime " << NthPrimeNumber(10001);

	_getch();
	return 0;
}